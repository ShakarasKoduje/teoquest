from django.core.management.base import BaseCommand, CommandError
import asyncio
import time
from .scraper import scraper
from .blogDataCreator import blogData
from .authorCreator import author_creator
import datetime
import time

class Command(BaseCommand):
    def handle(self, *args, **options):
        asyncio.run(self.main())

    async def main(self):
        print('Funkcja main rozpoczyna działanie')
        while True:
            t = datetime.datetime.now()
            #print(f"POCZATEK {t}")
            await scraper()
            await asyncio.sleep(5)
            await blogData()
            await asyncio.sleep(5)
            await author_creator()
            #await asyncio.sleep(180)
            for i in range(180):
                await asyncio.sleep(1)
            t = datetime.datetime.now()
            #print(f"KONIEC {t}")
