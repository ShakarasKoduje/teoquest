from teoApp.models import PostData, PostAuthor
import asyncio
from .analizatorTekstu import topTenAutora
import datetime

async def author_creator():
    t = datetime.datetime.now()
    #print(f"AUTHOR {t}")
    authors = PostData.objects.all()
    for a in authors:
        name = a.postAuthor
        print(name)
        try:
            #print("PROBA")
            postauthor = PostAuthor.objects.get(name = name)
            posts = PostData.objects.filter(postAuthor=postauthor.name)

            content = ""

            for p in posts:
                content += (str(f"{p.postContent}") + "\n\n\n\n")
            postauthor.posts = content
            topTen = topTenAutora(postauthor.name)
            if postauthor.topTen == topTen and postauthor.topTen != "":
                postauthor.save()
                #print("AUTOR IF")
            else:
                postauthor.topTen = topTen
                postauthor.save()
                
                #print("AUTOR ELSE")
            print(f"T: {postauthor.name}/{postauthor.topTen}")
        except PostAuthor.DoesNotExist:
            postauthor = PostAuthor()
            postauthor.name = name
            postauthor.save()
            print(f"E: {postauthor.name}/{postauthor.topTen}")
            await asyncio.sleep(0.01)
        
    await asyncio.sleep(1)
    t = datetime.datetime.now()
    #print(f"AUTOR KONIEC {t}")
