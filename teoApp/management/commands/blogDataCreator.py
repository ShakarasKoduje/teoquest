import requests

requests.packages.urllib3.disable_warnings()
import datetime
from teoApp.models import PostAuthor, PostContent, MyModel
from .analizatorTekstu import topTenBlog
import asyncio

async def blogData():
    print("BLOG")
    mm =MyModel()
    mm.name = f"utworzono {datetime.datetime.now()}"

    mm.save()
    authors = PostAuthor.objects.all()
    content = ""
    for a in authors:
        c = str(a.posts)
        content += c
    try:
        blogContent = PostContent.objects.get(id=1)
        blogContent.content = content
        topTen = topTenBlog(blogContent.id)
        print(topTen)

        blogContent.topTen = topTen
        blogContent.save()
        await asyncio.sleep(0.01)

    except PostContent.DoesNotExist:
        blogContent= PostContent(id=1)
        blogContent.content = content
        blogContent.save()
        await asyncio.sleep(0.01)
    await asyncio.sleep(1)