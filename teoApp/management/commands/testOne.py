from django.core.management.base import BaseCommand, CommandError

import asyncio
import time

class Command(BaseCommand):
    def handle(self, *args, **options):
        asyncio.run(self.main())

    async def powitanie(self):
        await asyncio.sleep(20)
        print(f"Witaj o {time.strftime('%X')}")

    async def main(self):
        print('Funkcja main rozpoczyna działanie')
        while True:
            await self.powitanie()