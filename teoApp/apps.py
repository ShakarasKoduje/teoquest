from django.apps import AppConfig


class TeoappConfig(AppConfig):
    name = 'teoApp'
